# Web-app

## What is this

This project is for a bigger automation project, this specific project is for install an web-app on a clean debian. But it's also able to work with packer, to build the web-app from just the debian image

## How to use

If you have a debian running, then the only thing you need is 
 - runme.sh
 - flask_pyez.py <br>
Then you just put them somewhere on the debian and run the runme.sh and then it's working

Otherwise you can use all of the files in the project, get download a debian image, and then run the packer script.

## Current status.

Right now everything is working and it's all ready to be depoyed