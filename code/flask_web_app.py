import sys # To be able to exit with a system code in case of an error

from flask import Flask

app = Flask('home')

@app.route('/')
def index():
    html = """
<h1>Hello World</h1>
"""
    return html
    
app.run(host='0.0.0.0')

