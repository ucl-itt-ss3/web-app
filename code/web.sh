#!/usr/bin/env python
import sys # To be able to exit with a system code in case of an error

from flask import Flask

# Pass in name to determine root path, then Flask can find other files easier
app = Flask(__name__)


# Route - mapping (connecting) a URL to Python function
@app.route('/')
def index():
    html = """
<h1>Hello World</h1>
"""
    return html
# Runs app only when we run this script directly, not if we import this somewhe$
if __name__ == "__main__":
    app.run(host='0.0.0.0')

